package com.skills421.drools.facts;

public class Person implements Fact
{
    public String country;
    public String name;
    public int age;

    public Person()
    {

    }

    public Person(String country,String name, int age)
    {
        this.country = country;
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString()
    {
        return String.format("Person [country=%s, name=%s, age=%s]", country,
                name, age);
    }
}
