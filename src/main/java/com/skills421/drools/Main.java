package com.skills421.drools;

import com.skills421.drools.controller.RuleRunner;
import com.skills421.drools.exceptions.RuleRunnerException;
import com.skills421.drools.facts.Person;
import com.skills421.drools.facts.RuleResponse;

public class Main
{

    public static void main(String[] args) throws RuleRunnerException
    {
        RuleRunner runner = new RuleRunner();

        RuleResponse response = runner
                .insertFact(new Person("USA","Jon Doe",21))
                .insertFact(new Person("USA","Jane Doe",19))
                .insertFact(new Person("UK","Mick Doe",19))
                .runRules("people-rules");

        for(String message : response.getMessages())
        {
            System.out.println(message);
        }
    }

}