package com.skills421.drools.utils;

import org.drools.core.io.impl.ClassPathResource;
import org.drools.decisiontable.InputType;
import org.drools.decisiontable.SpreadsheetCompiler;

import java.io.IOException;
import java.io.InputStream;

public class DecisionTableAnalyzer {
    public static void main(String[] args) throws IOException {
        String decisionTablePath = "com/skills421/drools/rules/drinkingrules.xls";

        InputStream is = new ClassPathResource(decisionTablePath).getInputStream();
        System.out.println(getDrl(is));
    }

    public static String getDrl(InputStream stream)
    {
        SpreadsheetCompiler comp = new SpreadsheetCompiler();
        String drl = comp.compile(false, stream, InputType.XLS);
        return drl;
    }
}
