package com.skills421.drools.exceptions;

public class RuleRunnerException extends Exception
{
    private static final long serialVersionUID = 1L;

    public RuleRunnerException()
    {
        super();
    }

    public RuleRunnerException(String message, Throwable cause,
                               boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public RuleRunnerException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public RuleRunnerException(String message)
    {
        super(message);
    }

    public RuleRunnerException(Throwable cause)
    {
        super(cause);
    }

}
